<cfsavecontent variable="myvar2"><?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <getPropertyDesc xmlns="http://www.instantsoftware.com/SecureWeblinkPlusAPI">
      <strUserId><cfoutput>#myuserid#</cfoutput></strUserId>
      <strPassword><Cfoutput>#mypassword#</Cfoutput></strPassword>
      <strCOID><Cfoutput>#mycoid#</Cfoutput></strCOID>
      <strPropId><cfoutput>#propid#</cfoutput></strPropId>
    </getPropertyDesc>
  </soap:Body>
</soap:Envelope></cfsavecontent>
<cfhttp url="https://secure.instantsoftwareonline.com/StayUSA/ChannelPartners/wsWeblinkPlusAPI.asmx" method="post">
  <cfhttpparam name="Host" type="HEADER" value="secure.instantsoftwareonline.com">
  <cfhttpparam name="Content-Type" type="HEADER" value="text/xml; charset=utf-8">
  <cfhttpparam name="SOAPAction" type="HEADER" value='"http://www.instantsoftware.com/SecureWeblinkPlusAPI/getPropertyDesc"'>
  <cfhttpparam name="" type="body" value="#myvar2#">
</cfhttp>
<cfoutput>
  <cfset myxml2 = xmlparse(#cfhttp.FileContent#)>
  <!---<cfdump var="#myxml2#">--->
  
  <Cfsavecontent variable="thisstuff">
  <!---<cfdump var="#myxml2#">--->
  </Cfsavecontent>
  
  <!---<cfdump var="#myxml2#">--->
  <cfset myattributes="">
  <cftry>
    <Cfloop index="i" from="1" to='#arraylen("#myxml2.envelope.body.getPropertyDescResponse.getPropertyDescResult.clsProperty.arrattributes.string#")#'>
      <cfset myattributes="#myattributes##myxml2.envelope.body.getPropertyDescResponse.getPropertyDescResult.clsProperty.arrattributes.string[i].xmltext#|">
    </Cfloop>
    <cfcatch>
    </cfcatch>
  </cftry>
  <cftry>
    <!---rates--->
    <Cfloop index="i" from="1" to='#arraylen("#myxml2.envelope.body.getPropertyDescResponse.getPropertyDescResult.clsProperty.arrSeasonRates.clsSeasonRates#")#'>
      <Cfquery name="addrates" datasource="#ds#">
insert into rentals_rates_dl (propid,seasonstart,seasonend,chargebasis,seasonrate) values ('#propid#',
#createodbcdate(dateformat("#listgetat("#myxml2.envelope.body.getpropertydescresponse.getpropertydescresult.clsproperty.arrseasonrates.clsseasonrates[i].dtbegindate.xmltext#", 1, "T")#", "yyyy-mm-dd"))#,
#createodbcdate(dateformat("#listgetat("#myxml2.envelope.body.getpropertydescresponse.getpropertydescresult.clsproperty.arrseasonrates.clsseasonrates[i].dtenddate.xmltext#", 1, "T")#", "yyyy-mm-dd"))#,
'#myxml2.envelope.body.getpropertydescresponse.getpropertydescresult.clsproperty.arrseasonrates.clsseasonrates[i].strchargebasis.xmltext#',
#myxml2.envelope.body.getpropertydescresponse.getpropertydescresult.clsproperty.arrseasonrates.clsseasonrates[i].dblrate.xmltext#)
</Cfquery>
    </Cfloop>
    <cfcatch>
      <cfoutput>#cfcatch.Detail#</cfoutput>
    </cfcatch>
  </cftry>
  
  <!---rental info--->
  <cfswitch expression="#myxml2.envelope.body.getpropertydescresponse.getpropertydescresult.clsproperty.arrTurnDayInfo.clsTurnDay.intTurnDay.xmltext#">
    <Cfcase value="1">
    <Cfset turnday="2">
    </Cfcase>
    <Cfcase value="2">
    <Cfset turnday="3">
    </Cfcase>
    <Cfcase value="3">
    <Cfset turnday="4">
    </Cfcase>
    <Cfcase value="4">
    <Cfset turnday="5">
    </Cfcase>
    <Cfcase value="5">
    <Cfset turnday="6">
    </Cfcase>
    <Cfcase value="6">
    <Cfset turnday="7">
    </Cfcase>
    <Cfcase value="7">
    <Cfset turnday="1">
    </Cfcase>
    <Cfcase value="0">
    <Cfset turnday="7">
    </Cfcase>
    <cfdefaultcase>
    <Cfset turnday="7">
    </cfdefaultcase>
  </cfswitch>
  
  <!---myxml2.envelope.body.getpropertydescresponse.getpropertydescresult.clsproperty.strname.xmltext--->
  <Cfquery name="ad" datasource="#ds#">
insert into rentals_dl (propid,propname,address1,address2,city,state,country,zip,phone,description,beds,baths,turnday,area,type,occupancy,rate,attributes,location,propertyheadline) values 
('#myxml2.envelope.body.getpropertydescresponse.getpropertydescresult.clsproperty.strpropid.xmltext#',
'#rtrim(listgetat(myxml2.envelope.body.getpropertydescresponse.getpropertydescresult.clsproperty.strname.xmltext,1,"["))#',
'#rtrim(listgetat(myxml2.envelope.body.getpropertydescresponse.getpropertydescresult.clsproperty.straddress1.xmltext,1,"["))#',
'#myxml2.envelope.body.getpropertydescresponse.getpropertydescresult.clsproperty.straddress2.xmltext#',
'#myxml2.envelope.body.getpropertydescresponse.getpropertydescresult.clsproperty.strcity.xmltext#',
'#myxml2.envelope.body.getpropertydescresponse.getpropertydescresult.clsproperty.strstate.xmltext#',
'#myxml2.envelope.body.getpropertydescresponse.getpropertydescresult.clsproperty.strcountry.xmltext#',
'#myxml2.envelope.body.getpropertydescresponse.getpropertydescresult.clsproperty.strzip.xmltext#',
'#myxml2.envelope.body.getpropertydescresponse.getpropertydescresult.clsproperty.strphone.xmltext#',
'#myxml2.envelope.body.getpropertydescresponse.getpropertydescresult.clsproperty.strdesc.xmltext#',
#myxml2.envelope.body.getpropertydescresponse.getpropertydescresult.clsproperty.dblbeds.xmltext#,
#myxml2.envelope.body.getpropertydescresponse.getpropertydescresult.clsproperty.dblbaths.xmltext#,
'#turnday#',
'#myxml2.envelope.body.getpropertydescresponse.getpropertydescresult.clsproperty.strarea.xmltext#',
'#myxml2.envelope.body.getpropertydescresponse.getpropertydescresult.clsproperty.strtype.xmltext#',
#myxml2.envelope.body.getpropertydescresponse.getpropertydescresult.clsproperty.intoccu.xmltext#,
'#myxml2.envelope.body.getpropertydescresponse.getpropertydescresult.clsproperty.dblrate.xmltext#',
'#myattributes#',
'#myxml2.envelope.body.getpropertydescresponse.getpropertydescresult.clsproperty.strlocation.xmltext#',
'#myxml2.envelope.body.getpropertydescresponse.getpropertydescresult.clsproperty.strPropertyHeadLine.xmltext#'
)


</Cfquery>
</cfoutput> 