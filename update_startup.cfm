<cfquery name="get" datasource="#ds#">
drop table if exists rentals_amenities_dl
</cfquery>
<cfquery name="create" datasource="#ds#">
create table `rentals_amenities_dl` (
  `id` int(11) not null auto_increment,
  `amenity` varchar(255) default null,
  primary key  (`id`)
)
</cfquery>
<cfquery name="get" datasource="#ds#">
drop table if exists rentals_area_dl
</cfquery>
<cfquery name="create" datasource="#ds#">
create table `rentals_area_dl` (
  `id` int(11) not null auto_increment,
  `code` varchar(255) default null,
  `area` varchar(255) default null,
  `count` int(11) default null,
  primary key  (`id`)
)
</cfquery>

<cfsavecontent variable="myvar5"><?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <getStartupInfo xmlns="http://www.instantsoftware.com/SecureWeblinkPlusAPI">
      <strUserId><cfoutput>#myuserid#</cfoutput></strUserId>
      <strPassword><Cfoutput>#mypassword#</Cfoutput></strPassword>
      <strCOID><Cfoutput>#mycoid#</Cfoutput></strCOID>
    </getStartupInfo>
  </soap:Body>
</soap:Envelope></cfsavecontent>
<cfhttp url="https://secure.instantsoftwareonline.com/StayUSA/ChannelPartners/wsWeblinkPlusAPI.asmx" method="post">
  <cfhttpparam name="Host" type="HEADER" value="secure.instantsoftwareonline.com">
  <cfhttpparam name="Content-Type" type="HEADER" value="text/xml; charset=utf-8">
  <cfhttpparam name="SOAPAction" type="HEADER" value='"http://www.instantsoftware.com/SecureWeblinkPlusAPI/getStartupInfo"'>
  <cfhttpparam name="" type="body" value="#myvar5#">
</cfhttp>
<cfoutput>
  <cfset myxml4 = xmlparse(#cfhttp.FileContent#)>
  <cfdump var="#myxml4#">
  <cftry>
    <Cfloop index="i" from="1" to='#arraylen("#myxml4.envelope.body.getStartupInfoResponse.getStartupInfoResult.arrStayUSAAttributesList.clsAttributeInfo#")#'>
      <cfquery name="add" datasource="#ds#">
insert into rentals_amenities_dl (amenity) values ('#myxml4.envelope.body.getStartupInfoResponse.getStartupInfoResult.arrStayUSAAttributesList.clsAttributeInfo[i].strAttrName.xmltext#')
</cfquery>
      '#myxml4.envelope.body.getStartupInfoResponse.getStartupInfoResult.arrStayUSAAttributesList.clsAttributeInfo[i].strAttrName.xmltext#'<br />
    </Cfloop>
    <cfcatch>
      ERROR<br />
      <cfdump var="#cfcatch#">
    </cfcatch>
  </cftry>
  <cftry>
    <Cfloop index="i" from="1" to='#arraylen("#myxml4.envelope.body.getStartupInfoResponse.getStartupInfoResult.areaList.clsAreaInfo#")#'>
      <cfquery name="add" datasource="#ds#">
insert into rentals_area_dl (code,area,count) values ('#myxml4.envelope.body.getStartupInfoResponse.getStartupInfoResult.areaList.clsAreaInfo[i].strCode.xmltext#',
'#myxml4.envelope.body.getStartupInfoResponse.getStartupInfoResult.areaList.clsAreaInfo[i].strAreaName.xmltext#',
#myxml4.envelope.body.getStartupInfoResponse.getStartupInfoResult.areaList.clsAreaInfo[i].intNoOfProps.xmltext#
)
</cfquery>
    </Cfloop>
    <cfcatch>
      ERROR<br />
      <cfdump var="#cfcatch#">
    </cfcatch>
  </cftry>
</cfoutput>
<cfquery name="drop" datasource="#ds#">
drop table if exists bk_rentals_amenities
</cfquery>
<cftry>
  <cfquery name="rename" datasource="#ds#">
rename table rentals_amenities to bk_rentals_amenities
</cfquery>
  <cfcatch>
    <cfoutput>#cfcatch.Detail#</cfoutput>
  </cfcatch>
</cftry>
<cftry>
  <cfquery name="rename" datasource="#ds#">
rename table rentals_amenities_dl to rentals_amenities
</cfquery>
  <cfcatch>
    <cfoutput>#cfcatch.Detail#</cfoutput>
  </cfcatch>
</cftry>
<cfquery name="drop" datasource="#ds#">
drop table if exists bk_rentals_area
</cfquery>
<cftry>
  <cfquery name="rename" datasource="#ds#">
rename table rentals_area to bk_rentals_area
</cfquery>
  <cfcatch>
    <cfoutput>#cfcatch.Detail#</cfoutput>
  </cfcatch>
</cftry>
<cftry>
  <cfquery name="rename" datasource="#ds#">
rename table rentals_area_dl to rentals_area
</cfquery>
  <cfcatch>
    <cfoutput>#cfcatch.Detail#</cfoutput>
  </cfcatch>
</cftry>
