<cfsetting requesttimeout="2000">
<cfquery name="get" datasource="#ds#">
drop table if exists rentals_avail_dl
</cfquery>
<cfquery name="create" datasource="#ds#">
create table `rentals_avail_dl` (
  `id` int(11) not null auto_increment,
  `arrivedate` date default null,
  `departdate` date default null,
  `staytype` varchar(255) default null,
  `propid` varchar(255) default null,
  primary key  (`id`)
)
</cfquery>
<Cfset errormessage="ERRORS:">
<Cfset mystart='#dateformat(now(), "yyyy-mm")#-01'>
<Cfset mystart="#dateformat("#dateadd("d",-90,mystart)#", "yyyy-mm-dd")#">
<Cfset myend="#dateformat("#dateadd("d",635,mystart)#", "yyyy-mm-dd")#">
<Cfquery name="get" datasource="#ds#">
select  propid from rentals
</Cfquery>

<Cfoutput query="get"> #propid# : #mystart# - #myend#<br />
  <cfsavecontent variable="myvar5"><?xml version="1.0" encoding="utf-8"?>
  <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Body>
      <getPropertyAvailabilityInfo xmlns="http://www.instantsoftware.com/SecureWeblinkPlusAPI">
        <strUserId>#myuserid#</strUserId>
        <strPassword>#mypassword#</strPassword>
        <strCOID>#mycoid#</strCOID>
        <strPropID>#propid#</strPropID>
      </getPropertyAvailabilityInfo>
    </soap:Body>
  </soap:Envelope></cfsavecontent>
  <cfhttp url="https://secure.instantsoftwareonline.com/StayUSA/ChannelPartners/wsWeblinkPlusAPI.asmx" method="post">
    <cfhttpparam name="Host" type="HEADER" value="secure.instantsoftwareonline.com">
    <cfhttpparam name="Content-Type" type="HEADER" value="text/xml; charset=utf-8">
    <cfhttpparam name="SOAPAction" type="HEADER" value='"http://www.instantsoftware.com/SecureWeblinkPlusAPI/getPropertyAvailabilityInfo"'>
    <cfhttpparam name="" type="body" value="#myvar5#">
  </cfhttp>
  <cfset myxml5 = xmlparse(#cfhttp.fileContent#)>
  <cftry>
    <Cftry>
      <cfif structKeyExists(myxml5.envelope.body.getPropertyAvailabilityInfoResponse.getPropertyAvailabilityInfoResult, "clsNonAvailDates" )>
        <Cfloop index="i" from="1" to='#arraylen("#myxml5.envelope.body.getPropertyAvailabilityInfoResponse.getPropertyAvailabilityInfoResult.clsNonAvailDates#")#'>
          <cftry>
            <Cfquery name="add" datasource="#ds#">
        insert into rentals_avail_dl (arrivedate,departdate,staytype,propid) values (
        #createodbcdate(dateformat("#listgetat("#myxml5.envelope.body.getPropertyAvailabilityInforesponse.getPropertyAvailabilityInforesult.clsNonAvailDates[i].dtFromDate.xmltext#", 1, "T")#", "yyyy-mm-dd"))#,
        #createodbcdate(dateformat("#listgetat("#myxml5.envelope.body.getPropertyAvailabilityInforesponse.getPropertyAvailabilityInforesult.clsNonAvailDates[i].dtToDate.xmltext#", 1, "T")#", "yyyy-mm-dd"))#,
        '#myxml5.envelope.body.getPropertyAvailabilityInforesponse.getPropertyAvailabilityInforesult.clsNonAvailDates[i].strstaytype.xmltext#','#propid#')
        </Cfquery>
            <cfcatch>
              <Cfoutput>
                <cfset deletetables=1>
                <cfsavecontent variable="errormessage">
                #errormessage#<br />
                ERROR1: #propid#<br />
                #cfcatch.Detail#<Br />
                <br />
                </cfsavecontent>
                ERROR1: #propid#<br />
                #cfcatch.Detail#<Br />
                <br />
              </Cfoutput>
            </cfcatch>
          </cftry>
        </Cfloop>
        <cfelse>
        #propid# - NO RESERVATIONS<BR>
        <br>
      </cfif>
      <cfcatch>
        <Cfoutput>
          <cfset deletetables=1>
          <cfsavecontent variable="errormessage">
          #errormessage#<br />
          ERROR2: #propid#<br />
          #cfcatch.Detail# #cfcatch.Message#<Br />
          <br />
          </cfsavecontent>
          ERROR2: #propid#<br />
          #cfcatch.Detail# #cfcatch.Message#<Br />
          <br />
          <cfdump var="#myxml5#">
        </Cfoutput>
      </cfcatch>
    </Cftry>
    
    <!---</CFIF>--->
    <Cfcatch>
      <cfsavecontent variable="errormessage">
      #errormessage#<br />
      <strong>#cfcatch.Detail# #cfcatch.Message#<br />
      </strong>
      </cfsavecontent>
      <strong>#cfcatch.Detail# #cfcatch.Message#<br />
      </strong>
      <cfset deletetables=1>
    </Cfcatch>
  </cftry>
</cfoutput>
<cfquery name="drop" datasource="#ds#">
drop table if exists bk_rentals_avail
</cfquery>
<CFSET sleep(5000)>
<cftry>
  <cfquery name="rename" datasource="#ds#">
rename table rentals_avail to bk_rentals_avail
</cfquery>
  <cfcatch>
    <cfoutput>#cfcatch.Detail#</cfoutput>
  </cfcatch>
</cftry>
<CFSET sleep(5000)>
<cftry>
  <cfquery name="rename" datasource="#ds#">
rename table rentals_avail_dl to rentals_avail
</cfquery>
  <cfcatch>
    <cfoutput>#cfcatch.Detail#</cfoutput>
  </cfcatch>
</cftry>
<CFIF NOT #IsDefined("deletetables")#>
  <cfelse>
  seaturtle update failed. <br />
  #errormessage#
</CFIF>
