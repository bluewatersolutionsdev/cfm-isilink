<cfsetting requesttimeout="2000">

<cftry>
  <cfsavecontent variable="myvar"><?xml version="1.0" encoding="utf-8"?>
  <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Body>
      <getPropertyIndexes xmlns="http://www.instantsoftware.com/SecureWeblinkPlusAPI">
        <strUserId><cfoutput>#myuserid#</cfoutput></strUserId>
        <strPassword><Cfoutput>#mypassword#</Cfoutput></strPassword>
        <strCOID><Cfoutput>#mycoid#</Cfoutput></strCOID>
      </getPropertyIndexes>
    </soap:Body>
  </soap:Envelope></cfsavecontent>
  <cfhttp url="https://secure.instantsoftwareonline.com/StayUSA/ChannelPartners/wsWeblinkPlusAPI.asmx" method="post">
    <cfhttpparam name="Host" type="HEADER" value="secure.instantsoftwareonline.com">
    <cfhttpparam name="Content-Type" type="HEADER" value="text/xml; charset=utf-8">
    <cfhttpparam name="SOAPAction" type="HEADER" value='"http://www.instantsoftware.com/SecureWeblinkPlusAPI/getPropertyIndexes"'>
    <cfhttpparam name="" type="body" value="#myvar#">
  </cfhttp>
  <cfset myxml = xmlparse(#cfhttp.FileContent#)>
  <Cfcatch>
    <strong>#cfcatch.Detail# #cfcatch.Message#<br />
    </strong>
    <cfset deletetables=1>
  </Cfcatch>
</cftry>
<cfif NOT #isdefined("url.dumponly")#>
  <cfquery name="get" datasource="#ds#">
drop table if exists rentals_dl
</cfquery>
  <cfquery name="create" datasource="#ds#">
create table `rentals_dl` (
  `id` int(11) not null auto_increment,
  `propid` varchar(255) default null,
  `propname` varchar(255) default null,
  `address1` varchar(255) default null,
  `address2` varchar(255) default null,
  `city` varchar(255) default null,
  `state` varchar(255) default null,
  `country` varchar(255) default null,
  `zip` varchar(255) default null,
  `phone` varchar(255) default null,
  `description` text,
  `beds` double default null,
  `baths` double default null,
  `turnday` varchar(255) default null,
  `area` varchar(255) default null,
  `location` varchar(255) default null,
  `type` varchar(255) default null,
  `occupancy` int(11) default null,
  `propertyheadline` varchar(255) default null,
  `rate` double default null,
  `attributes` text,
  primary key  (`id`),
  UNIQUE KEY `propid` (`propid`)
)
</cfquery>
  
  <!---rentals_rates table--->
  <cfquery name="get" datasource="#ds#">
drop table if exists rentals_rates_dl
</cfquery>
  <cfquery name="create" datasource="#ds#">
create table `rentals_rates_dl` (
  `id` int(11) not null auto_increment,
  `propid` varchar(255) default null,
  `seasonstart` date default null,
  `seasonend` date default null,
  `chargebasis` varchar(255) default null,
  `seasonrate` double default null,
	primary key  (`id`),
	key  (`propid`)
)
</cfquery>
</cfif>
<cfoutput>
  <CFIF #IsDefined("url.dumponly")#>
    <cfdump var="#myxml#">
    <cfabort>
  </CFIF>
  <Cfloop index="i" from="1" to='#arraylen("#myxml.envelope.body.getPropertyIndexesResponse.getPropertyIndexesResult.clspropindex#")#'>
    <cfset propid='#myxml.envelope.body.getPropertyIndexesResponse.getPropertyIndexesResult.clspropindex[i].strid.xmltext#'>
    <cftry>
      <cfinclude template="update_desc.cfm">
      <Cfcatch>
        <strong>#cfcatch.Detail# #cfcatch.Message#<br />
        </strong>
        <cfset deletetables=1>
      </Cfcatch>
    </cftry>
  </cfloop>
</cfoutput>
<cfquery datasource="#ds#" name="checkrecords">
select id from rentals_dl
</cfquery>
<cfif #checkrecords.recordcount# lt 1>
  <cfset deletetables=1>
  
    less than 1 records were found in rentals_dl
 
  <cfelse>
</cfif>
<CFIF NOT #IsDefined("deletetablestest")#>
  
  <!---rentals table--->
  <cfquery name="drop" datasource="#ds#">
drop table if exists bk_rentals
</cfquery>
  <cftry>
    <cfquery name="rename" datasource="#ds#">
rename table rentals to bk_rentals
</cfquery>
    <cfcatch>
      <cfoutput>#cfcatch.Detail#</cfoutput>
    </cfcatch>
  </cftry>
  <cftry>
    <cfquery name="rename" datasource="#ds#">
rename table rentals_dl to rentals
</cfquery>
    <cfcatch>
      <cfoutput>#cfcatch.Detail#</cfoutput>
    </cfcatch>
  </cftry>
  
  <!---rentals_rates table--->
  <cfquery name="drop" datasource="#ds#">
drop table if exists bk_rentals_rates
</cfquery>
  <cftry>
    <cfquery name="rename" datasource="#ds#">
rename table rentals_rates to bk_rentals_rates
</cfquery>
    <cfcatch>
      <cfoutput>#cfcatch.Detail#</cfoutput>
    </cfcatch>
  </cftry>
  <cftry>
    <cfquery name="rename" datasource="#ds#">
rename table rentals_rates_dl to rentals_rates
</cfquery>
    <cfcatch>
      <cfoutput>#cfcatch.Detail#</cfoutput>
    </cfcatch>
  </cftry>
  <cfelse>
  log error
</CFIF>
